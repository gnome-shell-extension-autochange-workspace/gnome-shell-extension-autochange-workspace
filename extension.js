// autochange-workspace is an extension which auto switch your workspace.
// Hit the pannel to activate it. Again will desactivate it.
// https://gitorious.org/gnome-shell-extension-autochange-workspace/gnome-shell-extension-autochange-workspace
//  TODO: Next step is to add a great button which would be used to activate it.
//
//  autochange-workspace is inspired by http://blog.fpmurphy.com/2011/04/gnome-3-shell-extensions.html
//  to INSTALL it, mv the folder in ~/.local/share/gnome-shell/extensions/autochange-workspace@shaiton.fedoraproject.org
const St = imports.gi.St;
const Mainloop = imports.mainloop;
const Lang = imports.lang;
const Main = imports.ui.main;
on = 0;


function switchWP() {
    this._init();
}

switchWP.prototype = {

    _init: function(){
        let on = 0;
	Main.panel.actor.reactive = true;
	Main.panel.actor.connect('button-release-event', this._startStop);
        here = this;
        Mainloop.timeout_add(300, function() {
            here._switch();
        })
    },

    _startStop: function(){
   	global.log("## _startStop");
        let text;

        if(!this.on){
	    this.on = 1;
            text = new St.Label({ style_class: 'autosw-label', text: "Autoswitch Workspace ON"});
        }else{
	    this.on = 0;
            text = new St.Label({ style_class: 'autosw-label', text: "Autoswitch Workspace OFF"});
        }
        let monitor = global.get_primary_monitor();
        global.stage.add_actor(text);
        text.set_position(Math.floor (monitor.width / 2 - text.width / 2), Math.floor(monitor.height / 2 - text.height / 2));
        Mainloop.timeout_add(1000, function () { text.destroy(); });

    },

    _switch: function(state){
   	//global.log("## on _switch()");
        if(on){
            let activeWorkspaceIndex = global.screen.get_active_workspace_index();
            let indexToActivate = activeWorkspaceIndex;
            if (activeWorkspaceIndex < global.screen.n_workspaces - 1)
                indexToActivate++;
            else
                indexToActivate = 0;

            if (indexToActivate != activeWorkspaceIndex)
                global.screen.get_workspace_by_index(indexToActivate).activate(global.get_current_time());
        }
	// infinite loop 
        Mainloop.timeout_add_seconds(7, Lang.bind(this, this._switch));

    },
}

// Put your extension initialization code here
function main() {  
    this._run = new switchWP();
}
